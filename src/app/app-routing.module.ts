import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ReviewPoComponent} from './components/review-po/review-po.component';
import {ReviewInvoicesComponent} from './components/review-invoices/review-invoices.component';
import {LoginComponent} from './components/login/login.component';

const routes: Routes = [
  {path: '', redirectTo: '/orders', pathMatch: 'full'},
  {path: 'orders', component: ReviewPoComponent},
  {path: 'invoices', component: ReviewInvoicesComponent},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
