import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReviewPoComponent } from './components/review-po/review-po.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { ReviewInvoicesComponent } from './components/review-invoices/review-invoices.component';
import {BsDatepickerModule, BsDropdownModule, ModalModule, TabsModule} from 'ngx-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RentitService} from './services/rentit.service';

@NgModule({
  declarations: [
    AppComponent,
    ReviewPoComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    ReviewInvoicesComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    TabsModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    HttpClientModule
  ],
  providers: [RentitService],
  bootstrap: [AppComponent]
})
export class AppModule { }
