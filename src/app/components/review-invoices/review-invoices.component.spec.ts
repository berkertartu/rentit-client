import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewInvoicesComponent } from './review-invoices.component';

describe('ReviewInvoicesComponent', () => {
  let component: ReviewInvoicesComponent;
  let fixture: ComponentFixture<ReviewInvoicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewInvoicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewInvoicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
