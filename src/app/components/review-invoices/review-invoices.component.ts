import { Component, OnInit } from '@angular/core';
import {RentitService} from '../../services/rentit.service';

@Component({
  selector: 'app-review-invoices',
  templateUrl: './review-invoices.component.html',
  styleUrls: ['./review-invoices.component.css']
})
export class ReviewInvoicesComponent implements OnInit {

  invoices: any = [];

  constructor(private rentitService : RentitService) { }

  ngOnInit() {
    this.getAllInvoices();
  }

  getAllInvoices() {
    this.rentitService.getAllInvoices().subscribe(res => {
      console.log(res);
      this.invoices = res;
    }, error => {

    });
  }

}
