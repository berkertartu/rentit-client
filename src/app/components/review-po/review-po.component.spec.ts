import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewPoComponent } from './review-po.component';

describe('ReviewPoComponent', () => {
  let component: ReviewPoComponent;
  let fixture: ComponentFixture<ReviewPoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReviewPoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReviewPoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
