import {Component, OnInit} from '@angular/core';
import {RentitService} from '../../services/rentit.service';

@Component({
  selector: 'app-review-po',
  templateUrl: './review-po.component.html',
  styleUrls: ['./review-po.component.css']
})
export class ReviewPoComponent implements OnInit {

  purchaseOrders: any = [];
  error: any;

  constructor(private rentitService: RentitService) {
  }

  ngOnInit() {
    this.getAllPO();
  }

  getAllPO() {
    this.rentitService.getAllPurchaseOrders().subscribe(res => {
      console.log(res);
      this.purchaseOrders = res;
    }, error => {
      this.error = error;
      console.log(error);
    });
  }

  makeRequestToUrl(url: string) {
    this.rentitService.genericRequest(url).subscribe(res => {
      console.log(res);
      this.getAllPO();
    }, error => {
    });
  }

}
