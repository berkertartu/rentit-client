import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()
export class RentitService {

  apiUrl = 'http://localhost:8080/api/v1/';

  constructor(private http: HttpClient) {
  }

  getAllPurchaseOrders() {
    return this.http.get(this.apiUrl + 'purchase-order');
  }

  getAllInvoices() {
    return this.http.get(this.apiUrl + 'invoice');
  }

  genericRequest(url) {
    return this.http.put(url, null);
  }

}
